package com.storm;


import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

import com.storm.bolts.BusyAntennaCounterBolt;
import com.storm.bolts.WordSpitterBolt;
import com.storm.spouts.LineReaderSpout;
import com.storm.bolts.TimeStampBolt;
import com.storm.bolts.RouteTracerBolt;
import com.storm.bolts.AvgTimePerTagBolt;

public class StormBringer {

	public static void main(String[] args) throws Exception {
		Config config = new Config();
		//config.put("inputFile", args[0]);
		//config.put("inputFile", "/home/thoris/Desktop/Storm Latest/test3.txt");   //Linux
		config.put("inputFile", "C:\\Users\\Bazooka\\Desktop\\test3.txt");   //Windows
                
		config.put(Config.TOPOLOGY_MAX_SPOUT_PENDING, 1);

		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("line-reader-spout", new LineReaderSpout()); //Dhmiourgia spout pou petaei sto stream line-reader-spout
		builder.setBolt("word-spitter", new WordSpitterBolt()).shuffleGrouping("line-reader-spout"); //Dhmiourgia bolt pou diavazei apo to stream line-reader-spout kai petaei sto stream word-spitter
		builder.setBolt("busy-antenna", new BusyAntennaCounterBolt()).shuffleGrouping("word-spitter","antennaStream");//to prwto e;inai to id tou opoiou akouei, to deutero einai to STREAM sto opoio akouei(mporei na exoume kai to default stream mono ara den tha uparxei to deutero orisma)Dhmiourgia bolt pou diavazei apo to stream word spitter kai petaei sto stream wordcounterstream
		builder.setBolt("timestamps", new TimeStampBolt()).shuffleGrouping("word-spitter", "timestampStream");//Bolt pou diavazei apo to stream wordspitter kai petaei sto stream averagesStream
		builder.setBolt("route",new RouteTracerBolt()).shuffleGrouping("word-spitter","routeStream");
		builder.setBolt("avgtimepertag",new AvgTimePerTagBolt()).shuffleGrouping("route","AvgTagStream");
		
		LocalCluster cluster = new LocalCluster();
		cluster.submitTopology("StormBringer", config, builder.createTopology());
		Thread.sleep(100000);

		cluster.shutdown();
	}

}
