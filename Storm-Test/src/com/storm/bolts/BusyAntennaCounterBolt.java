package com.storm.bolts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;

public class BusyAntennaCounterBolt implements IRichBolt{

	Integer id;
	String name;
	TreeMap<String, Integer> counters;   // orizoyme ws treemap thn ontothta poy metraei poses fores vrethikan oi antennas sto systhma
	private OutputCollector collector;
	
	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		this.counters = new TreeMap<String, Integer>();
		
		this.collector = collector;
		this.name = context.getThisComponentId();
		this.id = context.getThisTaskId();
		
	}
	
	@Override
	public void execute(Tuple input) {
		String str = input.getString(0);
		if(str.equals("KILL_SIG")){
			progress();
		}
		else{
			if(!counters.containsKey(str)){
				counters.put(str, 1);
				
			}else{
				Integer c = counters.get(str) +1;
				counters.put(str, c);
			}
		}
		collector.ack(input);
	}


	public void progress(){
		Set<Entry<String, Integer>> set = counters.entrySet(); // edw pername to entryset (diladi ola mas ta stoixeia - Key, Value) se ena set
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set); // kanoyme mia list me to set ayto kai xrhsimopoioyme ton overrided comparator 
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()             // (dhladh enan comparator me diki mas compare) gia na ta3inomisoyme ws pros Value
        		{ @Override
            public int compare(Map.Entry<String, Integer> o1,  Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());  // descending order 
                }
        });
       System.out.println("Progress: BusyAntennaCounterBolt says: \n I have received a KILL_SIG, until now the results are: \n"); 
       System.out.println("Progress: BusyAntennaCounterBolt says: Results coming from ["+ name + "-"+id +"]");
       
       System.out.println("Progress: BusyAntennaCounterBolt says: The following antennas was counted inside the room with descending order: ");
       for (int i=0; i<list.size(); i++)
       { System.out.print("\t \tProgress: " + list.get(i).getKey() + " was seen inside the room " + list.get(i).getValue() + " times until now\n");
       }  
	}
	
	
	
	@Override
	public void cleanup() {
		Set<Entry<String, Integer>> set = counters.entrySet(); // edw pername to entryset (diladi ola mas ta stoixeia - Key, Value) se ena set
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set); // kanoyme mia list me to set ayto kai xrhsimopoioyme ton overrided comparator 
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()             // (dhladh enan comparator me diki mas compare) gia na ta3inomisoyme ws pros Value
        		{ @Override
            public int compare(Map.Entry<String, Integer> o1,  Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());  // descending order 
                }
        });
        
       System.out.println("BusyAntennaCounterBolt says: Results coming from ["+ name + "-"+id +"]");
       
       System.out.println("BusyAntennaCounterBolt says: The following antennas was counted inside the room with descending order: ");
       for (int i=0; i<list.size(); i++)
       { System.out.print("\t \t" + list.get(i).getKey() + " was seen inside the room " + list.get(i).getValue() + " times \n");
       }  
		
	//	for(Map.Entry<String, Integer> entry:counters.entrySet()){
	//		System.out.println(entry.getKey()+" : " + entry.getValue());
	//	}
		System.out.println();
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
