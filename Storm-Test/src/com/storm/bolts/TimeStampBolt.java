package com.storm.bolts;

import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class TimeStampBolt implements IRichBolt {
private OutputCollector collector;
ArrayList<Float> timestamps = new ArrayList<Float>();
Map<String, Integer> counters;

	

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		this.collector = collector;
		
	}
	@Override
	public void execute(Tuple input) {
		String str_timestamp = input.getString(0);
		String[] parts = str_timestamp.split(":"); 
		int i;                                    
		float currdelaysum=0;
		float curravgdelay=0;
		int hours = Integer.parseInt(parts[0]);
		int minutes = Integer.parseInt(parts[1]);
		float seconds = Float.parseFloat(parts[2]);
		float timestamp_helper = (hours*60*60)+(minutes*60)+seconds;		
		if (timestamps.contains(timestamp_helper) == false ) //do not take duplicates
			timestamps.add((hours*60*60)+(minutes*60)+seconds);
		Collections.sort(timestamps);
		for (i = 0; i < timestamps.size(); i++) {
			//System.out.println("TimeStampBolt says: timestamps: "+timestamps.get(i));
			if((i+1)<timestamps.size()){     //an den eftasa akomh sto teleutaio tote upologise thn diafora apo to epomeno
				currdelaysum = currdelaysum + timestamps.get(i+1) - timestamps.get(i);
			} 
		}
		if(timestamps.size()!=1){//an exw mono ena timestamp mexri stigmhs mhn ypologiseis tipota
			curravgdelay=currdelaysum/(i-1);
		}
		System.out.println("TimeStampBolt says: Current average delay of tagged items' movements within the room is:  " +curravgdelay+ " seconds \n");
		collector.ack(input);
		
		
	 }
	

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
