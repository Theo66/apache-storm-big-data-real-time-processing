package com.storm.bolts;

import java.util.ArrayList;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class WordSpitterBolt implements IRichBolt{
	private OutputCollector collector;
	Map<Integer, Integer> counters;
	
	

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void execute(Tuple input) {
		String str_line = input.getString(0);
		ArrayList<String> route_inputs=new ArrayList<String>();
		
		if(str_line.equals("KILL_SIG")){
			System.out.println("Mphka \n \n");
			route_inputs.add(str_line);
			collector.emit("routeStream",new Values(route_inputs));
			collector.emit("antennaStream",new Values(str_line));
		}
		else{
		int i=0;
		String sentence = str_line.replace("\":", "");
		sentence = sentence.replace("\"", "");
		sentence = sentence.replace("{", "");
		sentence = sentence.replace("}", "");
		sentence = sentence.replace("input/", "");
		sentence = sentence.replace("tag", "");
		sentence = sentence.replace("timestamp", "");
		sentence = sentence.replace(",", "");
		String[] words = sentence.split(" ");
		for(String eachSplit: words){
			eachSplit = eachSplit.trim();
			if(!eachSplit.isEmpty()){
				System.out.println("WordSplitterBolt says: "+eachSplit);
				route_inputs.add(eachSplit);
				//route_inputs[i]=eachSplit;
				i=i+1;
				collector.emit("routeStream",new Values(route_inputs));
				
				if (eachSplit.contains("antenna")){   // arxikopoioyme to stream toy busyantennacounterbolt
					collector.emit("antennaStream",new Values(eachSplit));	
				}
				
				if (i%3==0){
					
					collector.emit("timestampStream",new Values(eachSplit));  //steile mou mono ta timestamps
				}
			}
			
		}
		}
		collector.ack(input);
		
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declareStream("antennaStream",new Fields("antennas"));    //uses default stream Id
		declarer.declareStream("timestampStream",new Fields("timestamps"));
		declarer.declareStream("routeStream",new Fields("eachSplit"));
		
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}
