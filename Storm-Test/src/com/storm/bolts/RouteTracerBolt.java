package com.storm.bolts;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.storm.Trace;

public class RouteTracerBolt implements IRichBolt{

	private OutputCollector collector;
	ArrayList<String> cleanup_array = new ArrayList<String>();
	ArrayList<Integer> max_tags=new ArrayList<Integer>(); //tha xrhsimeusei gia na kratame unique tags
	static int kill_counter=0;
	
	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		this.collector = collector;
        
	}
	
	@Override
	public void execute(Tuple input) {
		
		@SuppressWarnings("unchecked")
		ArrayList<String> temp_list = (ArrayList<String>)input.getValue(0);
		//System.out.println("Templistsize " + temp_list.size() +" exei mesa to " + temp_list.get(temp_list.size()-1));
		if(temp_list.contains("KILL_SIG")){
			kill_counter++;
			System.out.println("KILL_SIG "+ kill_counter);
			progress();	
			//removeAll(Collections.singleton("KILL_SIG"));
		    temp_list.clear();
			collector.ack(input);	
		}
		else if(temp_list.size()==0){
			collector.ack(input);
		}
		else
		{
			
		String[] temp_array=new String[temp_list.size()]; 
		for (int i=0; i< temp_list.size(); i++)
			temp_array[i]= temp_list.get(i);
		
		String[] parts = temp_array[2].split(":");
		int hours = Integer.parseInt(parts[0]);
		int minutes = Integer.parseInt(parts[1]);
		float seconds = Float.parseFloat(parts[2]);
		temp_array[2]=Float.toString((hours*60*60)+(minutes*60)+seconds);
		 
		 
		if (cleanup_array.contains(temp_array[2])==false)   // not to take duplicates or triplecates of the same inputs - Comparing timestamps
		 { 
		     Collections.addAll(cleanup_array, temp_array); // add temp_array to arraylist cleanup array, wste na exoyme ola ta stoixeia se mia arraylist global sthn class ayth
		     collector.ack(input);
		     collector.emit("AvgTagStream",new Values(cleanup_array));
		 }
		 else
			 collector.ack(input);
		}
		//collector.emit("AvgTagStream",new Values(cleanup_array));
	}

	@Override
	public void cleanup() {
	Trace[] trace_final = new Trace[cleanup_array.size()];
	int taghere;
	float stamphere;
        
		int j=0;
		for (int i=0; i<cleanup_array.size(); i=i+3)                   // gemizoyme to array of objects (classes) toy Trace
		{ 
			 taghere= Integer.parseInt(cleanup_array.get(i+1));
             stamphere = Float.parseFloat(cleanup_array.get(i+2));   
             trace_final[j] = new Trace(cleanup_array.get(i),taghere,stamphere);		  
             if(max_tags.contains(taghere)==false){    // unique number of tags
					max_tags.add(taghere);} 
             j=j+1;      //the number of objects/input lines   
		}
		collector.emit("AvgTagStream",new Values(cleanup_array));
		
		Collections.sort(max_tags);
		
		int i;
		int l;                                                         // sorting ws pros ta timestamps kai ta tags
		Trace trace_exchange =null;
		for (i=0; i<j; i++)
		{
			for (l=i+1; l<j; l++) 
			{	if (trace_final[i].get_tstamp()>trace_final[l].get_tstamp())
				{  
					trace_exchange = trace_final[i];
			        trace_final[i] = trace_final[l];
			        trace_final[l] = trace_exchange;
				}
				if (trace_final[i].get_tag()>trace_final[l].get_tag())
				{  
				    trace_exchange = trace_final[i];
			        trace_final[i] = trace_final[l];
			        trace_final[l] = trace_exchange;
				}
			}	
		}
		
		
		int k;
		int helptag;
		System.out.println();
		for (helptag=1; helptag<=max_tags.size(); helptag++) // printing the route of each tagged item.
		{	
			System.out.print("RouteTracerBolt says: The item tagged with " + max_tags.get(helptag-1) + " had been traced from the following antennas in that specific order:");
			for (k=0; k<j; k++)
				{	
				    
					if (trace_final[k].get_tag()==max_tags.get(helptag-1)){
					//System.out.print("^^^^^^ trace_final.get_tag " + trace_final[k].get_tag() + "max_tags.get(helptag-1_" + max_tags.get(helptag-1));
					  System.out.print(" --> "+ trace_final[k].get_antenna() + " ");  
					}
				}
				System.out.println();
		}			
			System.out.println();
			
			
		}
		
	

	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		declarer.declareStream("AvgTagStream",new Fields("cleanup_array"));
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void progress(){
		Trace[] trace_final = new Trace[cleanup_array.size()];
		int taghere;
		float stamphere;
	        
			int j=0;
			for (int i=0; i<cleanup_array.size(); i=i+3)                   // gemizoyme to array of objects (classes) toy Trace
			{ 
				 taghere= Integer.parseInt(cleanup_array.get(i+1));
	             stamphere = Float.parseFloat(cleanup_array.get(i+2));   
	             trace_final[j] = new Trace(cleanup_array.get(i),taghere,stamphere);		  
	             if(max_tags.contains(taghere)==false){    // unique number of tags
						max_tags.add(taghere);} 
	             j=j+1;      //the number of objects/input lines   
			}
			collector.emit("AvgTagStream",new Values(cleanup_array));
			
			Collections.sort(max_tags);
			
			int i;
			int l;                                                         // sorting ws pros ta timestamps kai ta tags
			Trace trace_exchange =null;
			for (i=0; i<j; i++)
			{
				for (l=i+1; l<j; l++) 
				{	if (trace_final[i].get_tstamp()>trace_final[l].get_tstamp())
					{  
						trace_exchange = trace_final[i];
				        trace_final[i] = trace_final[l];
				        trace_final[l] = trace_exchange;
					}
					if (trace_final[i].get_tag()>trace_final[l].get_tag())
					{  
					    trace_exchange = trace_final[i];
				        trace_final[i] = trace_final[l];
				        trace_final[l] = trace_exchange;
					}
				}	
			}
			
			
			int k;
			int helptag;
			System.out.println("Progress: RouteTracerBolt says: \n I have received a KILL_SIG, until now the results are: \n"); 
			for (helptag=1; helptag<=max_tags.size(); helptag++) // printing the route of each tagged item.
			{	
				System.out.print("RouteTracerBolt says: The item tagged with " + max_tags.get(helptag-1) + " had been traced from the following antennas in that specific order:");
				for (k=0; k<j; k++)
					{	
					    
						if (trace_final[k].get_tag()==max_tags.get(helptag-1)){
						//System.out.print("^^^^^^ trace_final.get_tag " + trace_final[k].get_tag() + "max_tags.get(helptag-1_" + max_tags.get(helptag-1));
						  System.out.print(" --> "+ trace_final[k].get_antenna() + " ");  
						}
					}
					System.out.println();
			}			
				System.out.println();
				
				
	}

}
