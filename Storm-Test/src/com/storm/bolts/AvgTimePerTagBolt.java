package com.storm.bolts;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;

import com.storm.Trace;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class AvgTimePerTagBolt implements IRichBolt {
	private OutputCollector collector;
	ArrayList<String> tracerinput = new ArrayList<String>();
	int counter=0;
	

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		this.collector = collector;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(Tuple input) {
		
		tracerinput = (ArrayList<String>)input.getValue(0);
		Trace[] trace_final = new Trace[tracerinput.size()];
		Trace[] bottlenecks = new Trace[tracerinput.size()];  //  nea array of Trace me abused stoixeia:   antenna_code (tha exei mesa thn plhroforia antenna pros antenna)
																//                                         tag (poios efage thn kinhsh),  
		                                                        //                                         timestamps (tha exei mesa ta time_diff ws pros ta opoia tha ginei kai h ta3inomisi)   
		
		int taghere;
		float stamphere;
	        
			int j=0;
			for (int i=0; i<tracerinput.size(); i=i+3)                   // gemizoyme to array of objects (classes) toy Trace
			{ 
				 taghere= Integer.parseInt(tracerinput.get(i+1));
	             stamphere = Float.parseFloat(tracerinput.get(i+2));   
	             trace_final[j] = new Trace(tracerinput.get(i),taghere,stamphere);		                                  
	             j=j+1;      //the number of objects/input lines teleiwnei me to megethos tou array.size
			}
			
			int i;
			int l;                                                         // sorting ws pros ta timestamps kai ta tags
			Trace trace_exchange =null;
			for (i=0; i<j; i++)
			{
				for (l=i+1; l<j; l++)                                                 // ascending order.                
				{	if (trace_final[i].get_tstamp()<trace_final[l].get_tstamp())
					{  
						trace_exchange = trace_final[i];
				        trace_final[i] = trace_final[l];
				        trace_final[l] = trace_exchange;
					}
					if (trace_final[i].get_tag()>trace_final[l].get_tag())
					{  
					    trace_exchange = trace_final[i];
				        trace_final[i] = trace_final[l];
				        trace_final[l] = trace_exchange;
					}
				}	
			}
			//int k;
			//for(k=0; k<j; k++)
			//   System.out.println("AvgTimePerTagBolt says: " + k + " has the " + trace_final[k].get_tag() + " with " + trace_final[k].get_tstamp());
			
			float temp_diff=0.0f;
			float sum=0.0f;
			int hop=0;
			int curr_tag=-1;
			int b_help=0;
			
			for(i=0; i<j;i++){
				curr_tag=trace_final[i].get_tag(); //kratame to tag gia to opoio meletame twra
				if((i+1)<j){   //tsekare an yparxei epomeno entry apo to twrino wste na kanoume diafora 
					if(trace_final[i].get_tag()==trace_final[i+1].get_tag()){ //an to tag tou twrinou einai idiou me tou epomenou kane diafora
						temp_diff=trace_final[i].get_tstamp()-trace_final[i+1].get_tstamp();
						sum=sum+temp_diff;
						hop=hop+1;
						bottlenecks[b_help]= new Trace(trace_final[i].get_antenna().concat(" --> ").concat(trace_final[i+1].get_antenna()),curr_tag,temp_diff);
						b_help++;
					}
					else if(hop!=0){ //alliws an den einai idio to epomeno tag, tote an metrhsame hops(den htan mono tou ena tag) vgale mesous orous
						sum=sum/hop;
						System.out.println("AvgTimePerTagBolt says: Average time per hops for tag "+curr_tag+" is "+sum+" seconds for "+ hop + " hops");						
						sum=0.0f;
						temp_diff=0.0f;
						hop=0;
					}
				}else if(hop!=0){
					sum=sum/hop;
					System.out.println("AvgTimePerTagBolt says: Average time per hops for tag "+curr_tag+" is "+sum+" seconds for "+ hop + " hops");
					sum=0.0f;
					temp_diff=0.0f;
					hop=0;
				}
			
			}
			System.out.println();
			
			int i2;
			int l2;                                                         // sorting ws pros ta timestamps kai ta tags
			Trace bottle_exchange =null;
			for (i2=0; i2<b_help; i2++)
			{
				for (l2=i2+1; l2<b_help; l2++)                                                 // ascending order.                
				{	if (bottlenecks[i2].get_tstamp()<bottlenecks[l2].get_tstamp())
					{  
						bottle_exchange = bottlenecks[i2];
						bottlenecks[i2] = bottlenecks[l2];
						bottlenecks[l2] = bottle_exchange;
					}
				}	
			}
			
			System.out.println();
			
			int d;
			for(d=0; d<b_help; d++)
			{
				System.out.println("AvgTimePerBolt says: Bottleneck incident no" + d + " was seen at the passage " + bottlenecks[d].get_antenna() + " for the tag: " + bottlenecks[d].get_tag() + " with delay:  " + bottlenecks[d].get_tstamp() + " seconds");
			}
			System.out.println();
			
		collector.ack(input);
		
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void declareOutputFields(final OutputFieldsDeclarer declarer) {
		
	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

}

