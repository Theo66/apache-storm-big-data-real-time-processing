package com.storm;



public class Trace {
    String antenna_c;
	int tag;
	float tstamp;
	
	public Trace (String antenna_c, int tag, float tstamp){                // class initializer
		this.antenna_c=antenna_c;
		this.tag=tag;
		this.tstamp=tstamp;
	}
	
	public void set_antenna(String a_c) {                                      // 3 set methods
		this.antenna_c = a_c;
	}
	public void set_tag(int t){
		this.tag=t;
	}
	public void set_tstamp(float ts){
	    this.tstamp=ts;
	}
	
    public String get_antenna(){                                     // 3 get methods
    	return this.antenna_c;
    }
    public int get_tag(){
    	return this.tag;
    }
    public float get_tstamp(){
    	return this.tstamp;
    }
       
}

	
	
