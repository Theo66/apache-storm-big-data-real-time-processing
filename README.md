# README #

This is the work done from Savvas Theodoros and Thanos Fotios as a part of their MSc Thesis.

### What is this repository for? ###

Real-time processing of Big Data from multiple network modules, RFID workflow and network analyzing, Apache Storm implementation in eclipse using JAVA. 
Dummy Input from files is provided. (Provided test3.txt for testing purposes in the form of the expected input)

### How do I get set up? ###

Import the eclipse project provided in the repository. 
Then, in the Storm_bringer.java file set an appropriate way to have an input stream. 
You can use the provided dummy input file test3.txt to see the correct form of input, 
or use the file by setting the correct path to read it in Storm_bringer.java.
The KILL_SIG signal, when read in the input stream is used to output relevant analytics and averages of the inputs up until then.
